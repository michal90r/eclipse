import { connect } from 'react-redux';
import { getItems } from 'store/actions';
import { Dashboard } from './components/Dashboard';

const mapStateToProps = ({ itemsReducer }) => ({
  items: itemsReducer.items,
  loading: itemsReducer.loading,
});

const mapDispatchToProps = (dispatch) => ({
  getItems: () => dispatch(getItems()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
