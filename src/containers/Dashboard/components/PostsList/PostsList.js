import React from "react";
import "./PostsList.scss";
import { Post } from "../Post"

export const PostsList = ({ items }) => {
  return (
    <div className="posts-list">
      {items.map((item, key) => (
        <div key={key} className="posts-list__post">
          <Post item={item} />
        </div>
      ))}
    </div>
  );
};
