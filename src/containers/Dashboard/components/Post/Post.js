import React from "react";
import "./Post.scss";

export const Post = ({
  item: {
    title,
    link,
    media: { m },
    published,
  },
}) => {
  //TODO : I would use the flickr API but didn't have enough time, + check if item is defined
  const profileHref = `https://www.flickr.com/photos/${link.split("/")[4]}/`;

  const openIframe = () => {
    // notice: We are not able to render flickr content inside iframe because x-frame-options response header is set to SAMEORIGIN
    window.open(`${window.location.origin}/post?link=${link}`, "_blank")
  };

  return (
    <div className="post">
      <img className="post__image" src={m} onClick={openIframe}/>

      <div className="post__links-and-text">
        <div className="post__title" onClick={openIframe}>{title}</div>


        <div className="post__links-panel">
          <span className="post__publication-date">{`Published: ${new Date(
              published
            ).toLocaleString()}`}</span>

          <a className="post__author"
            target="_blank"
            rel="noreferrer"
            href={profileHref}
          >
            Photo author
          </a>



          <a className="post__post-on-flickr"
             target="_blank"
             rel="noreferrer"
             href={link}
          >
            View on Flickr
          </a>

        </div>
      </div>
    </div>
  );
};