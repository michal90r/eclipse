import React, { useEffect } from "react";
import { PostsList } from "../PostsList";
import './Dashboard.scss'

export const Dashboard = ({ items, loading, getItems }) => {
  useEffect(() => {
    getItems();
  }, [getItems]);

  return (
    <div className="dashboard">
      {loading ? "loading" : <PostsList items={items} />}
    </div>
  );
};
