import React from "react";
import "./Iframe.scss";

export const Iframe = () => {

  const search = window.location.search;
  const params = new URLSearchParams(search);
  const link = params.get('link');

  return (
    <iframe src={link} className="iframe"/>
  );
};