import React from "react";
import { Provider } from "react-redux";
import { store } from "./store";
import { Dashboard } from "./containers/Dashboard";
import { Iframe } from "./common/components/Iframe";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Provider store={store}>
      <main>
        <Router>
          <Switch>
            <Route path="/post" component={Iframe} />
            <Route exact path="/" component={Dashboard} />
          </Switch>
        </Router>
      </main>
    </Provider>
  );
}

export default App;
