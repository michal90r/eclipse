import RestService from './RestService';

export const GET_ITEMS_REQUEST = 'GET_ITEMS_REQUEST';
export const GET_ITEMS_SUCCESS = 'GET_ITEMS_SUCCESS';
export const GET_ITEMS_ERROR = 'GET_ITEMS_ERROR';

const dataUrl = 'https://api.flickr.com/services/feeds/photos_public.gne?tags=space&tagmode=all&format=json&nojsoncallback=true';

const getItemsRequest = (payload) => ({
  type: GET_ITEMS_REQUEST,
  payload,
});

const getItemsSuccess = (payload) => ({
  type: GET_ITEMS_SUCCESS,
  payload,
});

const getItemsError = () => ({
  type: GET_ITEMS_ERROR,
});

export const getItems = () => {
  return function (dispatch) {
    dispatch(getItemsRequest());
    RestService.get(dataUrl)
      .then((response) => {
        dispatch(getItemsSuccess(response));
      })
      .catch(() => {
        dispatch(getItemsError());
      });
  };
};
