import {
  GET_ITEMS_REQUEST,
  GET_ITEMS_ERROR,
  GET_ITEMS_SUCCESS,
} from '../actions';

const initialState = {
  loading: false,
  error: false,
  items: [],
};

export function itemsReducer(
  state = initialState,
  { type, payload }
) {
  switch (type) {
    case GET_ITEMS_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case GET_ITEMS_SUCCESS: {
      const { data: { items } } = payload;

      return {
        ...state,
        loading: false,
        items
      };
    }
    case GET_ITEMS_ERROR: {
      return {
        ...state,
        loading: false,
      };
    }
    default: {
      return state;
    }
  }
}
